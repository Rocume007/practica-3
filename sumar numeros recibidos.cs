using System;

namespace recibir_numeros_y_sumarlos
{
    class Program
    {
        static void Main(string[] args)
        {
            int suma, sumador, cantidad;
            suma = 0;
            sumador = 0;
            do
            {
                Console.WriteLine("Ingresar numeros positivos, este programa se termina cuando se presione el 0 ");
                cantidad = int.Parse(Console.ReadLine());
                if (cantidad < 0)
                {
                    Console.WriteLine(" El numero introducido no es positivo");
                }
                else if (cantidad > 0)
                {

                    suma = suma + cantidad;
                    sumador++;
                    Console.WriteLine(" Espacio numero " + sumador + " la suma es igual a " + suma);

                }
            } while (cantidad != 0);
        }
    }
}
