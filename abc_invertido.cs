﻿using System;

namespace el_abc_invertido
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(" Este programa va proyectar el abecedario invertido :");
            Char ab = 'Z';
            do
            {
               Console.WriteLine(ab);
                ab--;
            } while (ab >= 'A');

        }
    }
}
