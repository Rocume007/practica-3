﻿using System;

namespace proyectar__del_1_al_10
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 1;
            do
            {
                Console.WriteLine(i);
                i++;
            }
            while (i < 11);
        }
    }
}
